FROM python:3.7-slim

COPY wheels wheels
RUN pip install wheels/*

COPY squad_ru_bert_infer.json squad_ru_bert_infer.json
COPY dp_components /root/.deeppavlov

COPY app.py app.py
CMD [ "python", "app.py" ]
