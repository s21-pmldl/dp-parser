TAG ?= registry.gitlab.com/s21-pmldl/dp-parser:local

.PHONY: docker-build
docker-build:
	export DOCKER_BUILDKIT=1 && docker build --tag ${TAG} --file Dockerfile . ${flags}

.PHONY: docker-build-no-cache
docker-build-no-cache:
	$(MAKE) build flags='--no-cache'
