from flask import Flask, request
from deeppavlov import build_model, configs

model = build_model("squad_ru_bert_infer.json", download=False)
app = Flask(__name__)

@app.route("/", methods=['POST'])
def parse():
    body = request.get_json()
    context = body["text"]
    context = "Напомни " + context
    result = model([context, context], ["Когда я просил напомнить?", "Что я просил напомнить?"])
    when, what = result[0]
    return {"when": when, "what": what}

app.run(host="0.0.0.0", port=3000)
